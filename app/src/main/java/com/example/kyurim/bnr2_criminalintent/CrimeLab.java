package com.example.kyurim.bnr2_criminalintent;

import android.content.Context;

import java.util.ArrayList;
import java.util.UUID;

public class CrimeLab {
  private ArrayList<Crime> mCrimes;

  // Singleton
  // store arraylist in a singleton - singleton is a class that allows only one instance of itself to be created
  // to create a singleton:
  // 1. create a class with a private constructor
  // 2. create get() method that returns the instance.
  //     - if the instance already exists, then get() method returns the instance.
  //     - if the instance does not exist, then get() method will call the constructor to create it.
  private static CrimeLab sCrimeLab;
  // Context parameter allows the singleton to start activities, access proj resources, find your app's private storage, and more.
  private Context mAppContext;

  private CrimeLab(Context appContext) {
    mAppContext = appContext;
    mCrimes = new ArrayList<Crime>();
    for (int i = 0; i < 100; i++) {
      Crime c = new Crime();
      c.setTitle("Crime #" + i);
      c.setSolved(i % 2 == 0);    // Every other one
      mCrimes.add(c);
    }
  }

  public static CrimeLab get(Context c) {
    if (sCrimeLab == null) {
      // to ensure that your singleton has a long-term Context to work with, call getApplicationContex() and trade the passed-in Context for the Application Context.
      // Application Context is a Context that is global to your application.
      // When you have application wide singleton, it should always use the Application Context.
      sCrimeLab = new CrimeLab(c.getApplicationContext());
    }
    return sCrimeLab;
  }

  public ArrayList<Crime> getmCrimes() {
    return mCrimes;
  }

  public Crime getCrimes(UUID id) {
    for (Crime c : mCrimes) {
      if (c.getId().equals(id)) {
        return c;
      }
    }
    return null;
  }
}
