package com.example.kyurim.bnr2_criminalintent;

// CrimeFragment is a Controller that interacts with Model and View objects.
// Main function is to present the details of a specific crime and update these details as the user changes them.

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import java.util.UUID;

public class CrimeFragment extends Fragment {
  public static final String EXTRA_CRIME_ID = "com.example.kyurim.bnr2_criminalintent.crime_id";
  private Crime mCrime;
  private EditText mTitleFieldEditText;
  private Button mDateButton;
  private CheckBox mSolvedCheckBox;

  public static CrimeFragment newInstance(UUID crimeId) {
    Bundle args = new Bundle();
    args.putSerializable(EXTRA_CRIME_ID, crimeId);

    CrimeFragment fragment = new CrimeFragment();
    fragment.setArguments(args);

    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Retrieving the extra and fetching the Crime
    UUID crimeId = (UUID)getArguments().getSerializable(EXTRA_CRIME_ID);    // getting crime ID from the arguments
    mCrime = CrimeLab.get(getActivity()).getCrimes(crimeId);
  }

  // Inflating the layout for Fragment's view
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_crime, parent, false);

  //mTitleFieldEditText = (EditText)findViewById(R.id.crime_title);               // Note that no "v" qualifier is needed for Activity!!!
    mTitleFieldEditText = (EditText)v.findViewById(R.id.crime_title_edittext);    // Note that "v" qualifier is needed for Fragment!!!
    mTitleFieldEditText.setText(mCrime.getTitle());           // display the Crime's data
    mTitleFieldEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // blank
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        mCrime.setTitle(s.toString());
      }

      @Override
      public void afterTextChanged(Editable s) {
        // blank
      }
    });

    mDateButton = (Button)v.findViewById(R.id.crime_date_button);
    mDateButton.setText(mCrime.getCustomDate());    // set the text of the Button dynamically instead of specifying on string.xml file
    mDateButton.setEnabled(false);                  // Button Disabled.  Will show as gray and will not be responsive.

    mSolvedCheckBox = (CheckBox)v.findViewById(R.id.crime_solved_checkbox);
    mSolvedCheckBox.setChecked(mCrime.isSolved());
    mSolvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        // Set the Crime Object's solved property
        mCrime.setSolved(isChecked);
      }
    });

    return v;
  }
}
