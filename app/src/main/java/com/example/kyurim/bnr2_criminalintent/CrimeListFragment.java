package com.example.kyurim.bnr2_criminalintent;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class CrimeListFragment extends ListFragment{
  private static final String TAG = "---- CrimeListFragment";
  private ArrayList<Crime> mCrimes;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getActivity().setTitle(R.string.crime_title);
    mCrimes = CrimeLab.get(getActivity()).getmCrimes();

    // ArrayAdapter<T> is an indirect subclass of an Adapter.
    // creating an ArrayAdaptor<T> and making it the adaptor for CrimeListFragment's ListView
//    ArrayAdapter<Crime> adapter = new ArrayAdapter<Crime>(getActivity(), android.R.layout.simple_list_item_1, mCrimes);
    // NOTE:  android.R.layout.simple_list_item_1 is a pre-defined laytout from resources provided by Android SDK.
    CrimeAdapter adapter = new CrimeAdapter(mCrimes);
    setListAdapter(adapter);    // this method is a ListFragment convenience method that you can use to set the adaptor of the implicit ListView managed by CrimeListFragemnt.
  }

  // Reloading the List - this will allow for updates on Crime Detailed View to be upated when the listView updates.
  public void onResume() {
    super.onResume();
    ((CrimeAdapter)getListAdapter()).notifyDataSetChanged();
  }

  // Responding to clicks on List itme.
  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    // Get the Crime from the Adapter
    Crime c = ((CrimeAdapter)getListAdapter()).getItem(position);
    Log.d(TAG, "Clicked:  " + c.getTitle());

    // Startup CrimePagerActivity with tihs crime
    Intent i = new Intent(getActivity(), CrimePagerActivity.class);
    i.putExtra(CrimeFragment.EXTRA_CRIME_ID, c.getId());        // .putExtra().  Tell CrimeFragment which Crime to display.
    startActivity(i);
  }

  // Creating an adaptor subclass.
  // Adding custom adapter as inner class.
  // The custom layout is designed to display Crime specific list items.
  // The data for these list items must be obtained using Crime accessor method, so you need a new adaptor that knows how to work with Crime objects.
  private class CrimeAdapter extends ArrayAdapter<Crime> {

    public CrimeAdapter(ArrayList<Crime> crimes) {
      // call to superclass constructor is required to properly hook up dataset of Crimes.
      super(getActivity(), 0, crimes);    // 0 - no pre-defined layout, so 0 for Layout ID
    }

    // getView() is Overrided to return a view inflated from the custom layout and populated with the correct Crime data.
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      // If we weren't given a view, inflate one
      if (convertView == null) {
        convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_crime, null);
      }

      // Configure the view for this Crime
      Crime c = getItem(position);      // get the correct Crime based on the index.
                                        // getItem() is a function of an Android Adapter object.

      TextView titleTextView = (TextView)convertView.findViewById(R.id.crime_list_item_title_TextView);
      titleTextView.setText(c.getTitle());

      TextView dateTextView = (TextView) convertView.findViewById(R.id.crime_list_item_dateTextView);
      dateTextView.setText(c.getDate().toString());

      CheckBox solvedCheckBox = (CheckBox)convertView.findViewById(R.id.crime_list_item_solved_CheckBox);
      solvedCheckBox.setChecked(c.isSolved());

      return convertView;
    }
  }
}
