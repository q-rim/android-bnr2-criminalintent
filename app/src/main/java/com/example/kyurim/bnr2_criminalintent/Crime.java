package com.example.kyurim.bnr2_criminalintent;

import java.util.Date;
import java.util.UUID;

public class Crime {
  private UUID mId;
  private String mTitle;
  private Date mDate;
  private boolean mSolved;

  public Crime() {
    // Generate unique identifier
    mId = UUID.randomUUID();
    mDate = new Date();
  }

  @Override
  public String toString() {

    return mTitle + ":  " + getCustomDate();
  }

  public UUID getId() {
    return mId;
  }

  public String getTitle() {
    return mTitle;
  }

  public void setTitle(String title) {
    mTitle = title;
  }

  public Date getDate() {
    return mDate;
  }

  public String getCustomDate() {
    String[] dateSplit = mDate.toString().split("\\s+");    // split date components.
    String[] timeSplit = dateSplit[3].split(":");           // change HH:MM:SS  into  HH:MM
    // Stitch together the values
    String date = timeSplit[0] + ":" + timeSplit[1] + "   " + dateSplit[0] +", "+ dateSplit[1] +" "+ dateSplit[2] + ", " + dateSplit[5];
    return date;
  }

  public void setDate(Date date) {
    mDate = date;
  }

  public boolean isSolved() {
    return mSolved;
  }

  public void setSolved(boolean solved) {
    mSolved = solved;
  }
}
