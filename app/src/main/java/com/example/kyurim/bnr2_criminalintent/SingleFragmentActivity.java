package com.example.kyurim.bnr2_criminalintent;

// Reuse of CrimeActivity.java code.
// This code will be reused many times, hence will be created or "stashed" in an abstract class.

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public abstract class SingleFragmentActivity extends FragmentActivity{

  protected abstract Fragment createFragment();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_fragment);

    android.app.FragmentManager fm = getFragmentManager();    // use for supporting API >= pre-Honeycomb
    Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);

    if (fragment == null) {
      fragment = createFragment();        // Create a new Fragment transaction -
      fm.beginTransaction().add(R.id.fragmentContainer, fragment).commit();     // add a Fragment and commits it.
      // .add(arg1: where the Fragment should appear,  arg2: Unique Identifier for Fragment in FragmentManager's List)
    }
  }
}
